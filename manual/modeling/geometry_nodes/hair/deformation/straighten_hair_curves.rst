.. index:: Geometry Nodes; Straighten Hair Curves

**********************
Straighten Hair Curves
**********************

Straightens hair curves between root and tip.

.. peertube:: grmo32udHpGWz2TwdEp9GD


Inputs
======

Geometry
   Input Geometry (only curves will be affected).

Amount
   Amount of straightening. Negative values will result in crumpling the curves.

Shape
   Shape of the influence along curves (0=constant, 0.5=linear).

Preserve Length
   Preserve each curve's length during deformation.


Properties
==========

This node has no properties.


Outputs
=======

**Geometry**
